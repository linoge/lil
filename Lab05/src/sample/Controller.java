package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    public ComboBox comboBox;
    public TextField textField;
    private final String REGEXP = "(\\s+(1{1,}|0{1,})){1,}";
    private final String FIRST_SEQUENCE = "((^[01]{5})(\\s|$))|((\\s[01]{5})(\\s|$))";
    public ListView list;

    public void addString(ActionEvent actionEvent) {

        String temp = " " + textField.getText();

        Pattern pattern = Pattern.compile(REGEXP);
        Matcher matcher = pattern.matcher(temp);
        if(matcher.matches()){
            comboBox.getItems().addAll(
                    textField.getText()
            );
        }
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Achtung!!!");
            alert.setHeaderText("Incorrect input");
            alert.setContentText("You must input groups of 1 and 0 without spaces in end of line");
            alert.showAndWait();
        }

    }

    public void findGroup(ActionEvent actionEvent) {
        String text = (String) comboBox.getValue();
        if(text != null) {
            Pattern pattern = Pattern.compile(FIRST_SEQUENCE);
            Matcher matcher = pattern.matcher(text);
            String tmp = "";
            if (matcher.find()) {
                tmp = matcher.group();
            }
            list.getItems().add(tmp);
        }
        else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Achtung!!!");
            alert.setHeaderText("Incorrect select");
            alert.setContentText("You must select any string");
            alert.showAndWait();
        }
    }
}
