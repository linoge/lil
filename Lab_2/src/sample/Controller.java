package sample;

import javafx.event.ActionEvent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ColorPicker;
import javafx.scene.paint.Color;

public class Controller {
    public Canvas canv;
    public ColorPicker color;
    public Color colorValue = Color.WHITE;
    public Color colorLine = Color.WHITE;
    public ColorPicker color1;

    public void test(ActionEvent actionEvent) {
        GraphicsContext gc = canv.getGraphicsContext2D();
        drawShapes(gc);

    }
    private void drawShapes(GraphicsContext gc) {
        gc.setFill(colorValue);
        gc.setStroke(Color.BLACK);
        gc.strokeLine(100, 100, 75, 150);
        gc.strokeLine(75, 150, 25, 175);
        gc.strokeLine(25, 175, 75,200);
        gc.strokeLine(75,200, 100,250);
        gc.strokeLine(100,250, 125,200);
        gc.strokeLine(125,200, 175, 175);
        gc.strokeLine(175, 175, 125,150);
        gc.strokeLine(125,150, 100, 100);


        gc.beginPath();
        gc.moveTo(100,100);
        gc.lineTo(75,150);
        gc.lineTo(25,175);
        gc.lineTo(75,200);
        gc.lineTo(100,250);
        gc.lineTo(125,200);
        gc.lineTo(175, 175);
        gc.lineTo(125,150);
        gc.fill();
    }

    public void color(ActionEvent actionEvent) {
        colorValue =  color.getValue();
        GraphicsContext gc = canv.getGraphicsContext2D();
        drawShapes(gc);
    }

    public void color1(ActionEvent actionEvent) {
        colorLine =  color1.getValue();
        GraphicsContext gc = canv.getGraphicsContext2D();
        drawShapes(gc);
    }
}
