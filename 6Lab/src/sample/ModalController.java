package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

/**
 * Created by Tim on 23.10.2016.
 */
public class ModalController {
    public TextField aValue;
    public TextField bValue;

    public void accept(ActionEvent actionEvent) {
        Double tempA = Double.parseDouble((aValue.getText()));
        Double tempB = Double.parseDouble(bValue.getText());
        if( (tempA < 0) || (tempB > (2 * Math.PI)) || (tempB <= tempA)){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Ahtung!!!!");
            alert.setHeaderText("Error");
            alert.setContentText("Check input numbers. It must between 1 and 2 * PI");

            alert.showAndWait();
        }
        else{
            Holder.setaValue(tempA);
            Holder.setbValue(tempB);
            Holder.getModalStage().close();
        }

    }
}
