package sample;

import javafx.stage.Stage;

/**
 * Created by Tim on 23.10.2016.
 */
public class Holder {

    private static Stage modalStage;
    private static Double aValue = 0.0;
    private static Double bValue = 1.0;
    private static Double nValue = 0.0;

    public static Stage getModalStage() {
        return modalStage;
    }

    public static void setModalStage(Stage modalStage) {
        Holder.modalStage = modalStage;
    }

    public static Double getaValue() {
        return aValue;
    }

    public static void setaValue(Double aValue) {
        Holder.aValue = aValue;
    }

    public static Double getbValue() {
        return bValue;
    }

    public static void setbValue(Double bValue) {
        Holder.bValue = bValue;
    }

    public static Double getnValue() {
        return nValue;
    }

    public static void setnValue(Double nValue) {
        Holder.nValue = nValue;
    }
}
