package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class Controller {
    public ProgressBar progress;
    public ListView list;
    public Slider slider;

    public void callModal(ActionEvent actionEvent) {
        Stage stage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("modal.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setScene(new Scene(root));
        stage.setTitle("My modal window");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node)actionEvent.getSource()).getScene().getWindow() );
        Holder.setModalStage(stage);
        stage.show();
    }

    public void calculate(ActionEvent actionEvent) {
        Holder.setnValue(slider.getValue());
        Double h = ( Holder.getbValue() - Holder.getaValue()) /  Holder.getnValue().intValue();
        Double summ = 0.0;
        for(int i = 1; i < Holder.getnValue().intValue(); i++){
            summ += Math.sin(h * i)*h;
        }
        list.getItems().addAll("A= " + Holder.getaValue() + "; B = " + Holder.getbValue() + "; Сумма равна: " + summ);
        progress.setProgress( 1.0);
    }
}
