package sample;

import javafx.stage.Stage;

/**
 * Created by Tim on 09.10.2016.
 */
public class Storage {
    private static Storage ourInstance = new Storage();
    private Stage primaryStage;
    private Double max =  - 99999.0;
    private Double min = 999999.0;
    private Model model = new Model();

    public static Storage getInstance() {
        return ourInstance;
    }

    private Storage() {
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public Model getModel() {
        return model;
    }
}
