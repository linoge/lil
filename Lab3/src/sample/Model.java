package sample;

/**
 * Created by Tim on 09.10.2016.
 */
public class Model {

    private Boolean max = false;
    private Boolean min = false;
    private Boolean isMaxUsed = false;
    private Boolean isMinUsed = false;
    private Double x = 0.0, y = 0.0;
    private Double funX = 0.0;

    public void setMax(Boolean max) {
        this.max = max;
    }

    public void setMin(Boolean min) {
        this.min = min;
    }

    public void setIsMaxUsed(){
        this.isMaxUsed = true;
    }

    public void setIsMinUsed(){
        this.isMinUsed = true;
    }

    public void setParams(Double x, Double y){
        this.x = x;
        this.y = y;
    }
    public String calculate(){
        Double result = 0.0;
        String resultText = "";
        Storage storage = Storage.getInstance();
        if( (x * y) > 0 ){
            result = positive();
        }
        else if( (x * y) < 0 ){
            result = negative();
        }
        else if( (x * y) == 0 ){
            result = equal();
        }
        if( max ){
            if( result > storage.getMax() ){
                storage.setMax(result);
            }

        }
        if( min ){
            if( result < storage.getMin() ){
                storage.setMin(result);
            }
        }

        resultText = "Результат равен: " + result.toString();

        if ( isMaxUsed ){
            resultText = resultText + ". Максимальное значение равно " + storage.getMax();
        }
        if ( isMinUsed ){
            resultText = resultText + ". Минимальное значение равно " + storage.getMin();
        }

        return resultText;
    }

    private Double positive(){
        Double tempRes = 0.0;
        tempRes = Math.sqrt(funX + y) - Math.sqrt(funX * y);
        return tempRes;
    }

    private Double negative(){
        Double tempRes = 0.0;
        tempRes = Math.sqrt(funX + y) + Math.abs(funX * y);
        return tempRes;
    }

    private Double equal(){
        Double tempRes = 0.0;
        tempRes = Math.sqrt(funX + y) + 1;
        return tempRes;
    }


    public void setFunX(Double funX) {
        this.funX = funX;
    }
}
