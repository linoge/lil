package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class Controller {
    public RadioButton cosXBtn;
    public RadioButton sinXBtn;
    public RadioButton tgXBtn;
    public TextField xValue;
    public TextField yValue;
    public TextArea area;
    public CheckBox min;
    public CheckBox max;

    public void setCos(ActionEvent actionEvent) {
        Model model = Storage.getInstance().getModel();
        model.setFunX(Math.cos(Double.parseDouble(xValue.getText())));
    }

    public void setSin(ActionEvent actionEvent) {
        Model model = Storage.getInstance().getModel();
        model.setFunX(Math.sin(Double.parseDouble(xValue.getText())));
    }

    public void setTg(ActionEvent actionEvent) {
        Model model = Storage.getInstance().getModel();
        model.setFunX(Math.tan(Double.parseDouble(xValue.getText())));
    }

    public void exit(ActionEvent actionEvent) {
        Storage storage = Storage.getInstance();
        storage.getPrimaryStage().close();
    }

    public void start(ActionEvent actionEvent) {
        Model model = Storage.getInstance().getModel();
        model.setParams(Double.parseDouble(xValue.getText()), Double.parseDouble(yValue.getText()));

        area.setText(area.getText() + "\n" + model.calculate());
    }

    public void setMin(ActionEvent actionEvent) {
        if(min.isSelected()){
            Storage.getInstance().getModel().setMin(true);
        }
        else{
            Storage.getInstance().getModel().setMin(false);
        }

        Storage.getInstance().getModel().setIsMinUsed();
    }

    public void setMax(ActionEvent actionEvent) {
        if(max.isSelected()){
            Storage.getInstance().getModel().setMax(true);
        }
        else{
            Storage.getInstance().getModel().setMax(false);
        }

        Storage.getInstance().getModel().setIsMaxUsed();
    }
}
