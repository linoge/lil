package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;

public class Controller {
    Model model;
    StageStorage storage;

    public void exit(ActionEvent actionEvent) {
        storage = StageStorage.getInstance();
        storage.getStage().close();
    }

    public void execute(ActionEvent actionEvent) {
        Double b = 5.0;
        model = new Model();
        model.setB(b);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Z1 = " + model.calculateZ1().toString() +
                            " Z2 = " +  model.calculateZ2().toString() +
                            " при B = " + b.toString()  );

        alert.showAndWait();
    }
}
