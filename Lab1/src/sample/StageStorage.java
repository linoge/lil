package sample;

import javafx.stage.Stage;

/**
 * Created by Tim on 09.10.2016.
 */
public class StageStorage {
    private static StageStorage ourInstance = new StageStorage();

    public static StageStorage getInstance() {
        return ourInstance;
    }

    private Stage stage;
    private StageStorage() {
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
