package sample;

/**
 * Created by Tim on 09.10.2016.
 */
public class Model {
    private Double b;
    private Double z1, z2;

    public void setB(Double b){
        this.b = b;
    }

    public Double calculateZ1(){
        Double temp = Math.sqrt(Math.pow(b, 2) - 4);
        z1 = Math.sqrt( (2 * b) + 2 * temp  ) / (temp + b +2);

        return z1;
    }

    public Double calculateZ2(){
        z2 = 1 / Math.sqrt(b + 2);
        return z2;
    }
}
